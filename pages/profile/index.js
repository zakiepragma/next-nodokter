import React from "react";
import {
  Avatar,
  Box,
  Button,
  Divider,
  Flex,
  Heading,
  SimpleGrid,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import Navbar from "../components/Navbar";
import Image from "next/image";

const userData = {
  name: "Al-Muzani",
  username: "almuzani",
  email: "almuzani@gmail.com",
  profilePicture: "https://bit.ly/dan-abramov",
  isMerchant: true,
  products: [
    {
      id: 1,
      name: "Product 1",
      description: "Lorem ipsum dolor sit amet",
      price: 100000,
      imageUrl: "https://via.placeholder.com/200x200.png?text=Product+1",
    },
    {
      id: 2,
      name: "Product 2",
      description: "Consectetur adipiscing elit",
      price: 200000,
      imageUrl: "https://via.placeholder.com/200x200.png?text=Product+2",
    },
  ],
};

export default function Profile() {
  const { name, username, email, profilePicture, isMerchant, products } =
    userData;

  return (
    <>
      <Navbar />
      <Box p={[4, 8]} bg={useColorModeValue("gray.50", "inherit")} minH="100vh">
        <Stack spacing={8}>
          <Flex
            alignItems="center"
            direction={["column", "col"]}
            justify="center"
          >
            <Avatar size="xl" name={name} src={profilePicture} mb={0} />
            <Stack spacing={1} mt={[0, 4]}>
              <Heading as="xl" size="xl" textAlign={["center", "left"]}>
                {name}
              </Heading>
              <Text fontSize="xl" color="gray.500">
                @{username}
              </Text>
              <Text fontSize="xl" color="gray.500">
                {email}
              </Text>
              <Button
                colorScheme="red"
                onClick={() => alert("Logout clicked!")}
              >
                Logout
              </Button>
            </Stack>
          </Flex>
          <Divider />

          {isMerchant && (
            <>
              <Flex alignItems="center" justify="space-between">
                <Heading as="h2" size="lg">
                  My Products
                </Heading>
                <Button
                  size="md"
                  colorScheme="teal"
                  onClick={() => alert("Create product clicked!")}
                >
                  Create
                </Button>
              </Flex>
              <SimpleGrid columns={[1, 2]} spacing={4}>
                {products.map((product) => (
                  <Box
                    key={product.id}
                    bg={useColorModeValue("white", "gray.700")}
                    boxShadow="md"
                    rounded="md"
                    p={4}
                  >
                    <Heading as="h3" size="md" mb={2}>
                      {product.name}
                    </Heading>
                    <Text fontSize="md" mb={4}>
                      {product.description}
                    </Text>
                    <Flex alignItems="center" justify="space-between">
                      <Text fontSize="lg" fontWeight="bold">
                        Rp {product.price}
                      </Text>
                      <Button size="sm">Edit</Button>
                    </Flex>
                  </Box>
                ))}
              </SimpleGrid>
            </>
          )}
        </Stack>
      </Box>
    </>
  );
}
