import Head from "next/head";
import Jumbotron from "./components/Jumbotron";
import Navbar from "./components/Navbar";
import Product from "./components/Product";

export default function Home() {
  return (
    <>
      <Head>
        <title>Next - noDokter</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <Jumbotron />
      <Product />
    </>
  );
}
