import React, { useState } from "react";
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { loginUser } from "../store";

export default function Login() {
  const router = useRouter();
  const dispatch = useDispatch();

  // State variables
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  // Event handler for form submission
  const handleSubmit = async (e) => {
    e.preventDefault();

    const userData = { email, password };

    try {
      // Dispatch login action
      await dispatch(loginUser(userData));

      // Redirect to home page on successful login
      router.push("/");
    } catch (error) {
      setError(error.response.data.message);
    }
  };

  return (
    <Box
      minH="100vh"
      py="12"
      px={{ base: "4", lg: "8" }}
      bg={useColorModeValue("gray.50", "inherit")}
    >
      <Box maxW="md" mx="auto">
        <Heading textAlign="center" size="xl" fontWeight="extrabold">
          Sign in to your account
        </Heading>

        {/* Link to registration page */}
        <Text mt="4" mb="8" align="center" maxW="md" fontWeight="medium">
          Don't have an account?{" "}
          <Box
            as="a"
            href="/register"
            color={useColorModeValue("teal.600", "teal.200")}
            display={{ base: "block", sm: "inline" }}
            marginLeft={1}
          >
            Sign up here
          </Box>
        </Text>

        {/* Login form */}
        <Box
          bg={useColorModeValue("white", "gray.700")}
          py="8"
          px={{ base: "4", md: "10" }}
          shadow="base"
          rounded={{ sm: "lg" }}
        >
          <form onSubmit={handleSubmit}>
            {error && (
              <Box mb="6" p="3" bg="red.100" rounded="md">
                {error}
              </Box>
            )}
            <Stack spacing="6">
              {/* Email input */}
              <FormControl id="email">
                <FormLabel>Email address</FormLabel>
                <Input
                  type="email"
                  autoComplete="email"
                  required
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </FormControl>

              {/* Password input */}
              <FormControl id="password">
                <FormLabel>Password</FormLabel>
                <Input
                  type="password"
                  autoComplete="current-password"
                  required
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </FormControl>

              {/* Submit button */}
              <Button type="submit" bg="#b2cdee" size="lg" fontSize="md">
                Sign in
              </Button>
            </Stack>
          </form>
        </Box>
      </Box>
    </Box>
  );
}
