import React, { useState } from "react";
import {
  Box,
  Button,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { useDispatch } from "react-redux";
import { registerUser } from "../store";
import { useRouter } from "next/router";

export default function Register() {
  const router = useRouter();
  const dispatch = useDispatch();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      setErrorMessage("Password and Confirm Password do not match");
      return;
    }
    const userData = {
      email,
      password,
      name,
    };
    try {
      await dispatch(registerUser(userData));
      router.push("/");
    } catch (error) {
      console.log(error);
      setErrorMessage(error.response.data.message);
    }
  };

  return (
    <Box
      minH="100vh"
      py="12"
      px={{ base: "4", lg: "8" }}
      bg={useColorModeValue("gray.50", "inherit")}
    >
      <Box maxW="md" mx="auto">
        <Heading textAlign="center" size="xl" fontWeight="extrabold">
          Create an account
        </Heading>
        <Text mt="4" mb="8" align="center" maxW="md" fontWeight="medium">
          <span>Already have an account?</span>
          <Box
            as="a"
            href="/login"
            color={useColorModeValue("teal.600", "teal.200")}
            display={{ base: "block", sm: "inline" }}
            marginLeft={1}
          >
            Sign in here
          </Box>
        </Text>
        <Box
          bg={useColorModeValue("white", "gray.700")}
          py="8"
          px={{ base: "4", md: "10" }}
          shadow="base"
          rounded={{ sm: "lg" }}
        >
          <form onSubmit={handleSubmit}>
            {errorMessage && (
              <Box mb="6" p="3" bg="red.100" rounded="md">
                {errorMessage}
              </Box>
            )}
            <Stack spacing="6">
              <FormControl id="name">
                <FormLabel>Name</FormLabel>
                <Input
                  type="text"
                  autoComplete="name"
                  required
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </FormControl>
              <FormControl id="email">
                <FormLabel>Email address</FormLabel>
                <Input
                  type="email"
                  autoComplete="email"
                  required
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </FormControl>
              <FormControl id="password">
                <FormLabel>Password</FormLabel>
                <Input
                  type="password"
                  autoComplete="new-password"
                  required
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </FormControl>
              <FormControl id="confirmPassword">
                <FormLabel>Confirm Password</FormLabel>
                <Input
                  type="password"
                  autoComplete="new-password"
                  required
                  value={confirmPassword}
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
              </FormControl>
              <Button type="submit" bg="#b2cdee" size="lg" fontSize="md">
                Create account
              </Button>
            </Stack>
          </form>
        </Box>
      </Box>
    </Box>
  );
}
