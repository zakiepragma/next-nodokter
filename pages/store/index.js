import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import axios from "axios";

// Initial state
const initialState = {
  loading: false,
  user: null,
  error: null,
};

// Action types
const REGISTER_USER_REQUEST = "REGISTER_USER_REQUEST";
const REGISTER_USER_SUCCESS = "REGISTER_USER_SUCCESS";
const REGISTER_USER_FAILURE = "REGISTER_USER_FAILURE";
const LOGIN_USER_REQUEST = "LOGIN_USER_REQUEST";
const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
const LOGIN_USER_FAILURE = "LOGIN_USER_FAILURE";

// Action creators
const registerUserRequest = () => ({ type: REGISTER_USER_REQUEST });
const registerUserSuccess = (user) => ({
  type: REGISTER_USER_SUCCESS,
  payload: user,
});
const registerUserFailure = (error) => ({
  type: REGISTER_USER_FAILURE,
  payload: error,
});
const loginUserRequest = () => ({ type: LOGIN_USER_REQUEST });
const loginUserSuccess = (user) => ({
  type: LOGIN_USER_SUCCESS,
  payload: user,
});
const loginUserFailure = (error) => ({
  type: LOGIN_USER_FAILURE,
  payload: error,
});

// Thunk actions
const registerUser = (userData) => {
  return async (dispatch) => {
    dispatch(registerUserRequest());
    try {
      const response = await axios.post(
        "http://localhost:4000/auth/register",
        userData
      );
      const user = response.data;
      dispatch(registerUserSuccess(user));
      return Promise.resolve(user);
    } catch (error) {
      dispatch(registerUserFailure(error.message));
      return Promise.reject(error);
    }
  };
};

const loginUser = (userData) => {
  return async (dispatch) => {
    dispatch(loginUserRequest());
    try {
      const response = await axios.post(
        "http://localhost:4000/auth/login",
        userData
      );
      const user = response.data;
      dispatch(loginUserSuccess(user));
      return Promise.resolve(user);
    } catch (error) {
      if (error.response && error.response.status === 401) {
        dispatch(loginUserFailure("Invalid email or password"));
      } else {
        dispatch(loginUserFailure(error.message));
      }
      return Promise.reject(error);
    }
  };
};

// Reducer
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_USER_REQUEST:
    case LOGIN_USER_REQUEST:
      return { ...state, loading: true };
    case REGISTER_USER_SUCCESS:
    case LOGIN_USER_SUCCESS:
      return { ...state, loading: false, user: action.payload };
    case REGISTER_USER_FAILURE:
    case LOGIN_USER_FAILURE:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};

// Store
const store = createStore(reducer, applyMiddleware(thunk));

export { registerUser, loginUser };
export default store;
