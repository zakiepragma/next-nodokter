import React from "react";
import { useSelector } from "react-redux";
import { Box, Button, Flex, Heading, Text } from "@chakra-ui/react";
import Link from "next/link";

export default function Home() {
  const user = useSelector((state) => state.user);
  console.log(user);
  return (
    <>
      <Box
        bgImage="url('/bg-health.jpg')"
        bgSize="cover"
        bgPosition="center"
        h="100vh"
        display="flex"
        justifyContent="center"
        alignItems="center"
        textAlign="center"
      >
        <Box p="4">
          <Heading as="h1" size="4xl" color="white">
            Healthy Products for a Better Life
          </Heading>
          <Text fontSize="2xl" color="white" mt="8">
            Shop now and improve your health with our quality products
          </Text>
          {user ? (
            <Heading
              as="h1"
              size="2xl"
              color="orange"
              textAlign="center"
              textShadow="2px 2px 8px rgba(0, 0, 0, 0.5)"
              mt="8"
            >
              Ahlan Wa Sahlan, {user.name}
            </Heading>
          ) : (
            <Flex justifyContent="center" alignItems="center" mt="8">
              <Link href="/login" passHref>
                <Button colorScheme="green" size="lg" mr="4">
                  Login
                </Button>
              </Link>
              <Link href="/register" passHref>
                <Button colorScheme="orange" size="lg" mr="4">
                  Register
                </Button>
              </Link>
            </Flex>
          )}
        </Box>
      </Box>
    </>
  );
}
