import { Box, Image, Text, Stack, Button, Flex } from "@chakra-ui/react";

const products = [
  {
    id: 1,
    name: "Product 1",
    price: "Rp 100.000",
    image: "https://via.placeholder.com/300x300",
  },
  {
    id: 2,
    name: "Product 2",
    price: "Rp 150.000",
    image: "https://via.placeholder.com/300x300",
  },
  {
    id: 3,
    name: "Product 3",
    price: "Rp 200.000",
    image: "https://via.placeholder.com/300x300",
  },
  {
    id: 4,
    name: "Product 4",
    price: "Rp 250.000",
    image: "https://via.placeholder.com/300x300",
  },
  {
    id: 5,
    name: "Product 5",
    price: "Rp 300.000",
    image: "https://via.placeholder.com/300x300",
  },
  {
    id: 6,
    name: "Product 6",
    price: "Rp 350.000",
    image: "https://via.placeholder.com/300x300",
  },
];

export default function Product() {
  return (
    <Box p={4}>
      <Stack
        direction={{ base: "column", md: "row" }}
        spacing={8}
        justifyContent="center"
        alignItems="center"
        flexWrap="wrap"
      >
        {products.map((product) => (
          <Box
            key={product.id}
            w={{ base: "100%", md: "22%" }}
            borderWidth="1px"
            borderRadius="lg"
            overflow="hidden"
            m={2}
          >
            <Image src={product.image} alt={product.name} />
            <Box p="6">
              <Box d="flex" alignItems="baseline">
                <Text fontWeight="semibold" fontSize="lg" mr="2">
                  {product.name}
                </Text>
                <Text color="gray.500" fontSize="sm">
                  {product.price}
                </Text>
              </Box>
              <Flex justify="end">
                <Button bg="#b2cdee" size="sm">
                  Add to Chart
                </Button>
              </Flex>
            </Box>
          </Box>
        ))}
      </Stack>
    </Box>
  );
}
