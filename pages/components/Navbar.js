import { useState } from "react";
import {
  Box,
  Flex,
  IconButton,
  Heading,
  useMediaQuery,
  Drawer,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  DrawerHeader,
  DrawerBody,
  Button,
  Icon,
} from "@chakra-ui/react";
import { FiUser, FiShoppingCart, FiMenu } from "react-icons/fi";
import Link from "next/link";

export default function Navbar() {
  const [showDrawer, setShowDrawer] = useState(false);
  const [isLargerThan768] = useMediaQuery("(min-width: 768px)");

  return (
    <>
      <Box p={4} bg="#b2cdee">
        <Flex justifyContent="space-between" alignItems="center">
          <Heading as="h1" size="lg" color="white">
            no-Dokter
          </Heading>
          {isLargerThan768 ? (
            <Flex>
              <Link href="/cart" passHref>
                <IconButton
                  as="a"
                  aria-label="Cart"
                  icon={<FiShoppingCart />}
                  colorScheme="teal"
                  variant="ghost"
                  mr={4}
                />
              </Link>
              <Link href="/profile" passHref>
                <IconButton
                  as="a"
                  aria-label="Profile"
                  icon={<FiUser />}
                  colorScheme="teal"
                  variant="ghost"
                />
              </Link>
            </Flex>
          ) : (
            <IconButton
              aria-label="Menu"
              icon={<FiMenu />}
              colorScheme="teal"
              variant="ghost"
              onClick={() => setShowDrawer(!showDrawer)}
            />
          )}
        </Flex>
      </Box>
      {!isLargerThan768 && (
        <Drawer isOpen={showDrawer} onClose={() => setShowDrawer(false)}>
          <DrawerOverlay>
            <DrawerContent>
              <DrawerCloseButton />
              <DrawerHeader>Menu</DrawerHeader>
              <DrawerBody>
                <Link href="/" passHref>
                  <Button
                    as="a"
                    variant="ghost"
                    w="100%"
                    mb={4}
                    onClick={() => setShowDrawer(false)}
                  >
                    <Flex alignItems="center">
                      <Icon as={FiShoppingCart} mr={2} />
                      Cart
                    </Flex>
                  </Button>
                </Link>
                <Link href="/profile" passHref>
                  <Button
                    as="a"
                    variant="ghost"
                    w="100%"
                    onClick={() => setShowDrawer(false)}
                  >
                    <Flex alignItems="center">
                      <Icon as={FiUser} mr={2} />
                      Profile
                    </Flex>
                  </Button>
                </Link>
              </DrawerBody>
            </DrawerContent>
          </DrawerOverlay>
        </Drawer>
      )}
    </>
  );
}
