import React from "react";
import {
  Box,
  Button,
  CloseButton,
  Flex,
  Heading,
  IconButton,
  Image,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { FaTrash } from "react-icons/fa";
import Navbar from "../components/Navbar";

const products = [
  {
    id: 1,
    name: "Product 1",
    price: "Rp. 150.000",
    image: "https://via.placeholder.com/150",
  },
  {
    id: 2,
    name: "Product 2",
    price: "Rp. 200.000",
    image: "https://via.placeholder.com/150",
  },
  {
    id: 3,
    name: "Product 3",
    price: "Rp. 300.000",
    image: "https://via.placeholder.com/150",
  },
];

export default function Cart() {
  const cartItems = products.slice(0, 2); // dummy cart items, first 2 products

  return (
    <>
      <Navbar />
      <Box
        minH="100vh"
        py="12"
        px={{ base: "4", lg: "8" }}
        bg={useColorModeValue("gray.50", "inherit")}
      >
        <Box maxW="md" mx="auto">
          <Heading textAlign="center" size="xl" fontWeight="extrabold">
            Shopping Cart
          </Heading>
          <Stack spacing="4" mt="8">
            {cartItems.map((item) => (
              <Flex
                key={item.id}
                direction={{ base: "column", md: "row" }}
                alignItems="center"
                justify="space-between"
                bg={useColorModeValue("white", "gray.700")}
                py="4"
                px={{ base: "4", md: "6" }}
                rounded={{ sm: "lg" }}
                shadow="base"
              >
                <Flex
                  alignItems="center"
                  mr={{ md: "4" }}
                  mb={{ base: "4", md: "0" }}
                >
                  <Image
                    src={item.image}
                    alt={item.name}
                    boxSize="150px"
                    objectFit="contain"
                  />
                  <Box
                    ml="4"
                    fontWeight="semibold"
                    maxW={{ base: "full", md: "20em" }}
                  >
                    <Text noOfLines={1}>{item.name}</Text>
                    <Text noOfLines={1} fontSize="sm" color="gray.600">
                      {item.price}
                    </Text>
                  </Box>
                </Flex>
                <Stack direction={{ base: "column", md: "row" }}>
                  <IconButton
                    aria-label="Remove item"
                    variant="ghost"
                    icon={<FaTrash />}
                    size="sm"
                    colorScheme="red"
                    mr={{ md: "4" }}
                  />
                  <CloseButton
                    aria-label="Remove item"
                    size="sm"
                    colorScheme="red"
                  />
                </Stack>
              </Flex>
            ))}
          </Stack>
          <Box mt="8">
            <Button bg="#b2cdee" size="lg" fontSize="md" isFullWidth>
              Checkout
            </Button>
          </Box>
        </Box>
      </Box>
    </>
  );
}
